#!/bin/sh

PACKAGE_REGISTRY_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"

function upload_pkg_file() {
    package=$1
    path=$2
    filename=$3

    curl -sS -f --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$path" "${PACKAGE_REGISTRY_URL}/${package}/${filename}"
}

PKG_VERSION=$1
MODULE_PACKAGE="${CI_PROJECT_NAME}/${PKG_VERSION}"

for FILE in ${CI_PROJECT_DIR}/bin/*
do
    upload_pkg_file "$MODULE_PACKAGE" "${FILE}" "$(basename ${FILE})"
done

# write version to a file to be consumed by dotenv
echo RELEASE_VERSION=${PKG_VERSION} > ${CI_PROJECT_DIR}/version.dotenv