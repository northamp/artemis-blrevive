package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/logutil"
	"gitlab.com/northamp/artemis-blrevive/pkg/blre"
	"go.uber.org/zap"
)

var (
	tempPath       string
	assetsPath     string
	blrPath        string
	blrePath       string
	blreConfigPath string
	modulesPath    string
	redistPath     string

	autoUpdate         bool
	fetchMissing       bool
	blreVersion        string
	blreRepo           string
	blrePackage        string
	serverutilsVersion string
	serverutilsRepo    string
	serverutilsPackage string
	redistVersion      string
	redistRepo         string
	redistPackage      string

	configName string
	serverName string
	password   string
	playlist   string
	gameMap    string
	gameMode   string
	// NOTE: the four parameters below could be integers, but they're strings on purpose
	// If they're int, they can't be nil and as such can't tell whether the user has set it to 0 or if it's unset
	// Those parameters end up in a string passed to BLRE anyway
	maxPlayers string
	numBots    string
	timeLimit  string
	scp        string
	custom     string
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Start a new Blacklight: Retribution Revive server",
	Long: `Artemis will start by checking for prerequisites appropriate to the OS, download missing dependencies if necessary, and then start a new BL:RE server.

Beware that modules such as server-utils may override some of the parameters passed from Artemis to the game!`,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
		// resolve relative paths in viper keys
		resolveRelViperPaths("Dirs.Temp", "Dirs.Assets")
		// resolve assets paths in viper keys
		resolveAssetsViperPaths(viper.GetString("Dirs.Assets"), "Dirs.BLR", "Dirs.BLRE", "Dirs.BLREConfig", "Dirs.Modules", "Dirs.Redist")
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		logutil.Logger.Debug("Start command called", zap.Any("settings", viper.AllSettings()))

		err := blre.StartServer()
		if err != nil {
			logutil.Logger.Fatal("An error occured", zap.Error(err))
		}
	},
}

func init() {
	rootCmd.AddCommand(startCmd)

	startCmd.Flags().StringVar(&tempPath, "dirs-temp", "", "path to where artemis' temporary dir should be created (defaults to system-defined)")
	startCmd.Flags().StringVar(&assetsPath, "dirs-assets", "./assets", "base path for all assets")
	startCmd.Flags().StringVar(&blrPath, "dirs-blr", "blr", "path to Blacklight: Retribution - expects to find at least `Binaries` and `FoxGame`")
	startCmd.Flags().StringVar(&blrePath, "dirs-blre", "blre", "path to Blacklight: Revive's DLLs")
	startCmd.Flags().StringVar(&blreConfigPath, "dirs-blreconfig", "config", "path to Blacklight: Revive's configuration")
	startCmd.Flags().StringVar(&modulesPath, "dirs-modules", "modules", "path to Blacklight: Revive's modules")
	startCmd.Flags().StringVar(&redistPath, "dirs-redist", "redist", "path to Blacklight: Revive's redistributables")

	startCmd.Flags().BoolVar(&autoUpdate, "assets-update", false, "forces assets update/download - overwrites existing data!")
	startCmd.Flags().BoolVar(&fetchMissing, "assets-fetch-missing", true, "downloads missing assets that are required by the server")
	startCmd.Flags().StringVar(&blreVersion, "assets-blre-version", "latest", "version of BL:RE that should be fetched")
	startCmd.Flags().StringVar(&blreRepo, "assets-blre-repository", "blrevive/blrevive", "repository where BL:RE that should be fetched")
	startCmd.Flags().StringVar(&blrePackage, "assets-blre-package", "blrevive", "name of the blrevive package")
	startCmd.Flags().StringVar(&serverutilsVersion, "assets-serverutils-version", "latest", "version of server-utils that should be fetched")
	startCmd.Flags().StringVar(&serverutilsRepo, "assets-serverutils-repository", "blrevive/modules/server-utils", "repository where server-utils that should be fetched")
	startCmd.Flags().StringVar(&serverutilsPackage, "assets-serverutils-package", "server-utils", "name of the server-utils package")
	startCmd.Flags().StringVar(&redistVersion, "assets-redist-version", "latest", "version of redistributables that should be fetched")
	startCmd.Flags().StringVar(&redistRepo, "assets-redist-repository", "northamp/blredistributables", "repository where redistributables that should be fetched")
	startCmd.Flags().StringVar(&redistPackage, "assets-redist-package", "blredistributables", "name of the redist package")

	startCmd.Flags().StringVar(&configName, "gamesettings-configname", "", "name of the configuration BLRevive should look for")
	startCmd.Flags().StringVar(&serverName, "gamesettings-servername", "", "advertised server name - should contain '+' instead of spaces when necessary!")
	startCmd.Flags().StringVar(&password, "gamesettings-password", "", "if set, players will need to enter this password to join")
	startCmd.Flags().StringVar(&playlist, "gamesettings-playlist", "", "playlist the server should run")
	startCmd.Flags().StringVar(&gameMap, "gamesettings-map", "", "if no playlist is set, server will use that map")
	startCmd.Flags().StringVar(&gameMode, "gamesettings-gamemode", "", "if no playlist is set, server will use that gamemode")
	startCmd.Flags().StringVar(&maxPlayers, "gamesettings-maxplayers", "", "max players the server should allow")
	startCmd.Flags().StringVar(&numBots, "gamesettings-numbots", "", "amount of bots the server should have")
	startCmd.Flags().StringVar(&timeLimit, "gamesettings-timelimit", "", "amount of time each rounds should take")
	startCmd.Flags().StringVar(&scp, "gamesettings-scp", "", "amount of SCP players should start with")
	startCmd.Flags().StringVar(&custom, "gamesettings-custom", "", "custom launch arguments passed to the server (?param=value?param2=value) - Dangerous!")

	viper.BindPFlag("Dirs.Temp", startCmd.Flags().Lookup("dirs-temp"))
	viper.BindPFlag("Dirs.Assets", startCmd.Flags().Lookup("dirs-assets"))
	viper.BindPFlag("Dirs.BLR", startCmd.Flags().Lookup("dirs-blr"))
	viper.BindPFlag("Dirs.BLRE", startCmd.Flags().Lookup("dirs-blre"))
	viper.BindPFlag("Dirs.BLREConfig", startCmd.Flags().Lookup("dirs-blreconfig"))
	viper.BindPFlag("Dirs.Modules", startCmd.Flags().Lookup("dirs-modules"))
	viper.BindPFlag("Dirs.Redist", startCmd.Flags().Lookup("dirs-redist"))

	viper.BindPFlag("Assets.Update", startCmd.Flags().Lookup("assets-update"))
	viper.BindPFlag("Assets.FetchMissing", startCmd.Flags().Lookup("assets-fetch-missing"))
	viper.BindPFlag("Assets.BLRE.Version", startCmd.Flags().Lookup("assets-blre-version"))
	viper.BindPFlag("Assets.BLRE.Repository", startCmd.Flags().Lookup("assets-blre-repository"))
	viper.BindPFlag("Assets.BLRE.Package", startCmd.Flags().Lookup("assets-blre-package"))
	viper.BindPFlag("Assets.ServerUtils.Version", startCmd.Flags().Lookup("assets-serverutils-version"))
	viper.BindPFlag("Assets.ServerUtils.Repository", startCmd.Flags().Lookup("assets-serverutils-repository"))
	viper.BindPFlag("Assets.ServerUtils.Package", startCmd.Flags().Lookup("assets-serverutils-package"))
	viper.BindPFlag("Assets.Redist.Version", startCmd.Flags().Lookup("assets-redist-version"))
	viper.BindPFlag("Assets.Redist.Repository", startCmd.Flags().Lookup("assets-redist-repository"))
	viper.BindPFlag("Assets.Redist.Package", startCmd.Flags().Lookup("assets-redist-package"))

	viper.BindPFlag("GameSettings.ConfigName", startCmd.Flags().Lookup("gamesettings-configname"))
	viper.BindPFlag("GameSettings.ServerName", startCmd.Flags().Lookup("gamesettings-servername"))
	viper.BindPFlag("GameSettings.Password", startCmd.Flags().Lookup("gamesettings-password"))
	viper.BindPFlag("GameSettings.Playlist", startCmd.Flags().Lookup("gamesettings-playlist"))
	viper.BindPFlag("GameSettings.Map", startCmd.Flags().Lookup("gamesettings-map"))
	viper.BindPFlag("GameSettings.GameMode", startCmd.Flags().Lookup("gamesettings-gamemode"))
	viper.BindPFlag("GameSettings.MaxPlayers", startCmd.Flags().Lookup("gamesettings-maxplayers"))
	viper.BindPFlag("GameSettings.NumBots", startCmd.Flags().Lookup("gamesettings-numbots"))
	viper.BindPFlag("GameSettings.TimeLimit", startCmd.Flags().Lookup("gamesettings-timelimit"))
	viper.BindPFlag("GameSettings.SCP", startCmd.Flags().Lookup("gamesettings-scp"))
	viper.BindPFlag("GameSettings.Custom", startCmd.Flags().Lookup("gamesettings-custom"))
}
