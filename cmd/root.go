package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/logutil"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/version"
	"go.uber.org/zap"
)

var (
	cfgFile   string
	debug     bool
	logJson   bool
	logOutput string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "artemis",
	Short: "Artemis - a Blacklight: Retribution Revive project server launcher",
	Long: `Artemis is a server launcher for the Blacklight: Retribution Revive project.

It aims at reducing the setup overhead for server instances on multiple platforms (essentially Windows and Linux OS),
and also facilitate administration with features such as log-aggregation and console-binding`,
	CompletionOptions: cobra.CompletionOptions{DisableDefaultCmd: true},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	cobra.OnFinalize(cleanup)

	rootCmd.CompletionOptions.DisableDefaultCmd = true

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file Artemis should use")
	rootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "start Artemis in debug mode, helps with troubleshooting")
	rootCmd.PersistentFlags().BoolVar(&logJson, "log-json", false, "output logs as JSON")
	rootCmd.PersistentFlags().StringVar(&logOutput, "log-output", "", "write logs to a file as well as the console if a valid file path is set")

	viper.BindPFlag("Config", rootCmd.PersistentFlags().Lookup("config"))
	viper.BindPFlag("Debug", rootCmd.PersistentFlags().Lookup("debug"))
	viper.BindPFlag("Log.Json", rootCmd.PersistentFlags().Lookup("json"))
	viper.BindPFlag("Log.Output", rootCmd.PersistentFlags().Lookup("log-output"))
}

// Basic configuration for Viper
func initConfig() {
	// Read from env vars
	viper.SetEnvPrefix("BLREVIVE")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
	if f := viper.GetString("Config"); f != "" {
		viper.SetConfigFile(f)
	}

	// Read from config if it exists
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			fmt.Printf("an error occurred while reading the config file: %s", err)
			os.Exit(-1)
		}
	}

	// Set up logging
	if err := configureLogging(); err != nil {
		fmt.Printf("failed to configure logging: %s", err)
		os.Exit(-1)
	}
}

// Configure internal logger
func configureLogging() error {
	var config zap.Config
	if viper.GetBool("Log.Json") {
		config = zap.NewProductionConfig()
	} else {
		config = zap.NewDevelopmentConfig()
	}

	if viper.GetBool("Debug") {
		config.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	} else {
		config.DisableCaller = true
		config.DisableStacktrace = true
		config.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
	}

	if lo := viper.GetString("Log.Output"); lo != "" {
		config.OutputPaths = append(config.OutputPaths, lo)
	}

	if err := logutil.InitLogger(config); err != nil {
		return err
	}

	logutil.Logger.Info("Artemis started", zap.String("version", version.GetVersion()))
	return nil
}

// Only run sync on the logger upon leaving
func cleanup() {
	logutil.Logger.Sync()
}
