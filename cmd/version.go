package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/logutil"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/version"
	"go.uber.org/zap"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Prints various version information",
	Long:  `Depending on how Artemis has been built, this command prints out various version information such as the Git tag, commit hash, build date, etc...`,
	Run: func(cmd *cobra.Command, args []string) {
		logutil.Logger.Debug("Version command called", zap.Any("settings", viper.AllSettings()))

		v := version.Get()

		fmt.Printf("Artemis Version Information:\n")
		fmt.Printf("  Version:      %s\n", v.Version)
		fmt.Printf("  Git Commit:   %s\n", v.GitCommit)
		fmt.Printf("  Git Tree State: %s\n", v.GitTreeState)
		fmt.Printf("  Go Version:   %s\n", v.GoVersion)
		fmt.Printf("  Build Date:   %s\n", v.BuildDate)
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
