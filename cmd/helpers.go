package cmd

// Miscellaneous helpers for Cobra commands

import (
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/logutil"
	"go.uber.org/zap"
)

// Resolves relative assets path based on Dirs.Assets
//
//	Rules:
//		/blre 	= /blre
//		./blre 	= $CWD/blre
//		blre 	= {Dirs.Assets}/blre
func resolveAssetsViperPaths(assetsPath string, keys ...string) {
	for _, key := range keys {
		path := viper.GetString(key)
		absPath := path

		if filepath.IsLocal(path) && !strings.HasPrefix(path, ".") {
			absPath = filepath.Join(assetsPath, path)
		}

		absPath = getAbsPathOrFail(absPath)
		viper.Set(key, absPath)
	}
}

// Resolves relative to absolute paths in viper keys
func resolveRelViperPaths(keys ...string) {
	for _, key := range keys {
		relpath := viper.GetString(key)
		if relpath != "" {
			abspath := getAbsPathOrFail(relpath)
			viper.Set(key, abspath)
		}
	}
}

// shorthand for filepath.Abs to fail and exit on error
func getAbsPathOrFail(path string) string {
	abspath, err := filepath.Abs(path)
	if err != nil {
		logutil.Logger.Fatal("failed to resolve absolute path", zap.String("Path", path), zap.Error(err))
	}
	return abspath
}
