// Stripped version.go from Helm - an Apache 2.0 licensed software (886e626)
// https://github.com/helm/helm/blob/abdbe1ed342021e1eefc086e9f0b403c1167c41f/internal/version/version.go

package version

import (
	"flag"
	"runtime"
	"strconv"
	"time"
)

var (
	// version is Artemis' current version
	version = "dev"
	// metadata is extra build time data
	metadata = ""
	// gitCommit is the git commit ID
	gitCommit = ""
	// gitTreeState is the state of the git tree
	gitTreeState = ""
	// buildTimestamp is a timestamp of the build
	buildTimestamp = ""
)

// BuildInfo describes the compile time information
type BuildInfo struct {
	// Version is the current semver
	Version string
	// GitCommit is the git commit ID
	GitCommit string
	// GitTreeState is the state of the git tree
	GitTreeState string
	// GoVersion is the version of the Go compiler used
	GoVersion string
	// BuildDate is an Unix timestamp when the build was made
	BuildTimestamp string
	// BuildDate is the date of the build computed on the fly from BuildTimestamp
	BuildDate string
}

// GetVersion returns the semver string of the version
func GetVersion() string {
	if metadata == "" {
		return version
	}
	return version + "+" + metadata
}

func computeBuildDate() (date string) {
	if buildTimestamp == "" {
		return "unknown"
	}

	i, err := strconv.ParseInt(buildTimestamp, 10, 64)
	if err != nil {
		return "invalid"
	}
	return time.Unix(i, 0).Format(time.RFC3339)
}

// Get returns build info
func Get() BuildInfo {
	v := BuildInfo{
		Version:        GetVersion(),
		GitCommit:      gitCommit,
		GitTreeState:   gitTreeState,
		GoVersion:      runtime.Version(),
		BuildTimestamp: buildTimestamp,
		BuildDate:      computeBuildDate(),
	}

	if flag.Lookup("test.v") != nil {
		v.GoVersion = ""
	}
	return v
}
