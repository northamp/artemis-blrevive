package downloadutil

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/logutil"
	"go.uber.org/zap"
)

type GitlabDownloader struct {
	gitlabClient *gitlab.Client
}

// Initializes a new Gitlab client, could implement more options at a later date if necessary
func NewGitlabDownloader(token string) (g GitlabDownloader, err error) {
	g.gitlabClient, err = gitlab.NewClient(token)
	if err != nil {
		return g, err
	}
	return g, nil
}

// Returns a specific package's whereabouts
func (c GitlabDownloader) getPackageInfo(projectId string, packageName string, version string) (*gitlab.Package, error) {
	listOpts := gitlab.ListProjectPackagesOptions{
		PackageName: &packageName,
		Sort:        gitlab.Ptr("desc"),
		OrderBy:     gitlab.Ptr("version"),
	}

	if version != "latest" {
		listOpts.PackageVersion = &version
	}

	packages, _, err := c.gitlabClient.Packages.ListProjectPackages(projectId, &listOpts)
	if err != nil {
		return nil, err
	}

	if len(packages) == 0 {
		return nil, fmt.Errorf("could not find package %s in project %s for version %s", packageName, projectId, version)
	}

	return packages[0], nil
}

// Downloads all or specific files from a package
func (c GitlabDownloader) downloadPackageFiles(pid string, pkg *gitlab.Package, downloadDir string, files []string) (downloadedFiles []string, err error) {
	if len(files) == 0 {
		pkgFiles, _, err := c.gitlabClient.Packages.ListPackageFiles(pid, pkg.ID, nil)
		if err != nil {
			return nil, err
		}

		for _, pkgFile := range pkgFiles {
			files = append(files, pkgFile.FileName)
		}
	}

	for _, file := range files {
		logutil.Logger.Debug("Downloading file from package", zap.String("file", file), zap.String("package", pkg.Name), zap.String("version", pkg.Version))
		fileContent, _, err := c.gitlabClient.GenericPackages.DownloadPackageFile(pid, pkg.Name, pkg.Version, file)
		if err != nil {
			return nil, err
		}

		err = os.WriteFile(filepath.Join(downloadDir, file), fileContent, 0664)
		if err != nil {
			return nil, err
		}
		downloadedFiles = append(downloadedFiles, file)
	}

	return downloadedFiles, nil
}

// Downloads files from a Gitlab package
func (c GitlabDownloader) DownloadPackageFiles(projectName string, packageName string, version string, downloadDir string, files []string) (downloadedFiles []string, err error) {
	if _, err = os.Stat(downloadDir); os.IsNotExist(err) {
		if err = os.MkdirAll(downloadDir, os.ModePerm); err != nil {
			return nil, fmt.Errorf("failed to create directory at %s", downloadDir)
		}
	}

	pkg, err := c.getPackageInfo(projectName, packageName, version)
	if err != nil {
		return nil, err
	}

	logutil.Logger.Debug("Package found", zap.String("version", pkg.Version), zap.String("projectName", projectName), zap.String("packageName", packageName))

	downloadedFiles, err = c.downloadPackageFiles(projectName, pkg, downloadDir, files)
	if err != nil {
		return nil, fmt.Errorf("failed to download package: %s", err)
	}

	return downloadedFiles, nil
}
