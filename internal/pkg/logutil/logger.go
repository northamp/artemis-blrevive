package logutil

import (
	"go.uber.org/zap"
)

var Logger *zap.Logger

func InitLogger(config zap.Config) (err error) {
	Logger, err = config.Build()
	return err
}
