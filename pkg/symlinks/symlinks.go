package symlinks

import (
	"os"
	"path/filepath"

	"gitlab.com/northamp/artemis-blrevive/internal/pkg/logutil"
	"go.uber.org/zap"
)

// Create symbolic links from sourceDir to targetDir for the files listed in fileList
func CreateLinks(fileList []string, sourceDir string, targetDir string) error {
	logutil.Logger.Debug("Creating links", zap.Strings("fileList", fileList), zap.String("sourceDir", sourceDir), zap.String("targetDir", targetDir))

	for _, relativePath := range fileList {
		sourcePath := filepath.Join(sourceDir, relativePath)
		targetPath := filepath.Join(targetDir, relativePath)

		// Ensure the source file or directory exists
		_, err := os.Stat(sourcePath)
		if err != nil {
			return err
		}

		// Check if it's a directory
		isDir, err := isDirectory(sourcePath)
		if err != nil {
			return err
		}

		if isDir {
			// Handle directory separately
			err = handleDirectory(sourcePath, targetPath)
			if err != nil {
				return err
			}
		} else {
			// Handle file
			err = createLink(sourcePath, targetPath)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// Calls os.Stat on the path and tells whether it's a dir or not
func isDirectory(path string) (bool, error) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false, err
	}
	return fileInfo.IsDir(), nil
}

func handleDirectory(sourcePath, targetPath string) error {
	// Recursively create links for files within the directory
	err := filepath.Walk(sourcePath, func(filePath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Skip directories as they are handled separately
		if info.IsDir() {
			return nil
		}

		relativePath, err := filepath.Rel(sourcePath, filePath)
		if err != nil {
			return err
		}

		targetFilePath := filepath.Join(targetPath, relativePath)
		return createLink(filePath, targetFilePath)
	})

	if err != nil {
		return err
	}

	return nil
}

func createLink(sourcePath, targetPath string) error {
	// Create the directory structure in the target directory
	err := os.MkdirAll(filepath.Dir(targetPath), os.ModePerm)
	if err != nil {
		return err
	}

	// Note: Will require admin rights on Windows...
	err = os.Symlink(sourcePath, targetPath)
	if err != nil {
		return err
	}

	logutil.Logger.Debug("Created link", zap.String("sourcePath", sourcePath), zap.String("targetPath", targetPath))

	return nil
}
