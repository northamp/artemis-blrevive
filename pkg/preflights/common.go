package preflights

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/viper"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/downloadutil"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/ioutils"
	"gitlab.com/northamp/artemis-blrevive/internal/pkg/logutil"
	"gitlab.com/northamp/artemis-blrevive/pkg/symlinks"
	"go.uber.org/zap"
)

// Returns the list of Blacklight: Retribution resources that are needed for the server to work
func getGameResourcesList() []string {
	return []string{
		"Binaries/Win32/FoxGame-win32-Shipping.exe",
		"Binaries/Win32/libeay32.dll",
		"Binaries/Win32/libogg.dll",
		"Binaries/Win32/libvorbis.dll",
		"Binaries/Win32/libvorbisfile.dll",
		"Binaries/Win32/cudart32_41_22.dll",
		"Binaries/Win32/PhysXCooking.dll",
		"Binaries/Win32/PhysXCore.dll",
		"Binaries/Win32/PhysXLoader.dll",
		"Binaries/Win32/ssleay32.dll",
		"FoxGame/CookedPCConsole/",
		"FoxGame/Config/PCConsole/Cooked",
	}
}

// Returns the list of redistributables that are needed for the server to work
func getRedistsList() []string {
	return []string{
		"steam_api.dll",
		"Steam.dll",
		"steamclient.dll",
		"tier0_s.dll",
		"vstdlib_s.dll",
	}
}

// Returns the list of Blacklight: Revive resources that are needed for the server to work
func getBlreResourcesList() []string {
	return []string{
		"DINPUT8.dll",
		"BLRevive.dll",
	}
}

// Handles platform agnostic preflight
func Common() (err error) {
	if viper.GetBool("Assets.Update") {
		if err = FetchAssets(true); err != nil {
			return err
		}
	} else if viper.GetBool("Assets.FetchMissing") {
		if err = FetchAssets(viper.GetBool("Assets.Update")); err != nil {
			return err
		}
	}

	// Symlink the game resources
	err = symlinks.CreateLinks(getGameResourcesList(), viper.GetString("Dirs.BLR"), viper.GetString("TempDir"))
	if err != nil {
		return fmt.Errorf("an error occured while linking the game resources: %w", err)
	}

	// Symlink the redists
	err = symlinks.CreateLinks(getRedistsList(), viper.GetString("Dirs.Redist"), filepath.Join(viper.GetString("TempDir"), "Binaries", "Win32"))
	if err != nil {
		return fmt.Errorf("an error occured while linking redists: %w", err)
	}

	// Symlink BLRE
	err = symlinks.CreateLinks(getBlreResourcesList(), viper.GetString("Dirs.BLRE"), filepath.Join(viper.GetString("TempDir"), "Binaries", "Win32"))
	if err != nil {
		return fmt.Errorf("an error occured while linking BLRE resources: %w", err)
	}

	// Create Modules in ephemeral dir and symlink modules from Modules
	modulesDir := filepath.Join(viper.GetString("TempDir"), "Binaries", "Win32", "Modules")
	err = os.MkdirAll(modulesDir, os.ModePerm)
	if err != nil {
		return err
	}

	symlinkedModules, err := filepath.Glob(filepath.Join(viper.GetString("Dirs.Modules"), "*.dll"))
	if err != nil {
		return err
	}
	// Remove full path from Glob's return
	for i, match := range symlinkedModules {
		symlinkedModules[i] = filepath.Base(match)
	}
	logutil.Logger.Debug("Found modules", zap.String("modulesDir", viper.GetString("Dirs.Modules")), zap.Strings("symlinkedModules", symlinkedModules))

	if symlinkedModules != nil {
		err = symlinks.CreateLinks(symlinkedModules, viper.GetString("Dirs.Modules"), modulesDir)
		if err != nil {
			return fmt.Errorf("an error occured while linking modules: %w", err)
		}
	}

	// Create BLRevive in the ephemeral dir and symlink confs there
	configDir := filepath.Join(viper.GetString("TempDir"), "FoxGame", "Config")
	if err = os.MkdirAll(filepath.Join(configDir, "BLRevive"), os.ModePerm); err != nil {
		return err
	}

	symlinkedConfigs, err := filepath.Glob(filepath.Join(viper.GetString("Dirs.BLREConfig"), "*"))
	if err != nil {
		return err
	}
	// Remove full path from Glob's return
	for i, match := range symlinkedConfigs {
		symlinkedConfigs[i] = filepath.Base(match)
	}
	logutil.Logger.Debug("Found configs", zap.Strings("symlinkedConfigs", symlinkedConfigs))

	if symlinkedConfigs != nil {
		err = symlinks.CreateLinks(symlinkedConfigs, viper.GetString("Dirs.BLREConfig"), configDir)
		if err != nil {
			return fmt.Errorf("an error occured while linking configurations: %w", err)
		}
	}

	// Create Logs dir
	logsDir := filepath.Join(viper.GetString("TempDir"), "FoxGame", "Logs")
	err = os.MkdirAll(logsDir, os.ModePerm)
	if err != nil {
		return err
	}

	return nil
}

type GitlabAssetInfo struct {
	Key         string
	Repository  string
	PackageName string
	Version     string
}

func GitlabAssetInfoFromViper(viperKey string) (assetInfo GitlabAssetInfo) {
	return GitlabAssetInfo{
		Key:         viperKey,
		Repository:  viper.GetString(fmt.Sprintf("Assets.%s.Repository", viperKey)),
		PackageName: viper.GetString(fmt.Sprintf("Assets.%s.Package", viperKey)),
		Version:     viper.GetString(fmt.Sprintf("Assets.%s.Version", viperKey)),
	}
}

// Pulls BLRE-related assets
func FetchAssets(overwrite bool) (err error) {
	gitlabDownloader, err := downloadutil.NewGitlabDownloader("")
	if err != nil {
		return err
	}

	blreAssetInfo := GitlabAssetInfoFromViper("BLRE")
	if err = FetchAsset(blreAssetInfo, viper.GetString("Dirs.BLRE"), overwrite, gitlabDownloader); err != nil {
		return err
	}

	redistAssetInfo := GitlabAssetInfoFromViper("Redist")
	if err = FetchAsset(redistAssetInfo, viper.GetString("Dirs.Redist"), overwrite, gitlabDownloader); err != nil {
		return err
	}

	serverUtilsAssetInfo := GitlabAssetInfoFromViper("ServerUtils")
	if err = FetchAsset(serverUtilsAssetInfo, viper.GetString("Dirs.Modules"), overwrite, gitlabDownloader); err != nil {
		return err
	}

	return nil
}

func FetchAsset(assetInfo GitlabAssetInfo, dest string, overwrite bool, gitlabDownloader downloadutil.GitlabDownloader) (err error) {
	// create dest directory if it doesn't exist
	if _, err := os.Stat(dest); os.IsNotExist(err) {
		if err = os.MkdirAll(dest, os.ModePerm); err != nil {
			return fmt.Errorf("failed to create %s assets directory at %s: %w", assetInfo.Key, dest, err)
		}
	}

	// download package files if destination dir is empty or overwrite flag given
	if ioutils.DirIsEmpty(dest) || overwrite {
		var downloadedFiles []string
		if downloadedFiles, err = gitlabDownloader.DownloadPackageFiles(assetInfo.Repository, assetInfo.PackageName, assetInfo.Version, dest, nil); err != nil {
			return fmt.Errorf("could not download %s from %s: %w", assetInfo.Key, assetInfo.Repository, err)
		}

		logutil.Logger.Info(fmt.Sprintf("Downloaded %s successfully", assetInfo.PackageName),
			zap.String(fmt.Sprintf("Assets.%s.Version", assetInfo.Key), assetInfo.Version),
			zap.String(fmt.Sprintf("Dirs.%s", assetInfo.Key), dest),
			zap.String("projectName", assetInfo.Repository),
			zap.String("packageName", assetInfo.PackageName),
			zap.Strings("downloadedFiles", downloadedFiles),
		)
	}

	return nil
}
