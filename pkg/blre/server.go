package blre

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"github.com/fsnotify/fsnotify"
	"github.com/nxadm/tail"
	"github.com/spf13/viper"
	"go.uber.org/zap"

	"gitlab.com/northamp/artemis-blrevive/internal/pkg/logutil"
	"gitlab.com/northamp/artemis-blrevive/pkg/preflights"
)

var wg sync.WaitGroup

func StartServer() (err error) {
	// Create temp dir
	tempDir, err := os.MkdirTemp(viper.GetString("Dirs.Temp"), "artemis")
	if err != nil {
		return fmt.Errorf("unable to create temp dir: %w", err)
	}

	viper.Set("TempDir", tempDir)

	logutil.Logger.Debug("Temporary dir created", zap.String("TempDir", viper.GetString("TempDir")))

	// Defer temp dir removal if debug mode is not on
	if !viper.GetBool("Debug") {
		defer os.RemoveAll(viper.GetString("TempDir"))
	}

	// Run preflights - see preflight package
	logutil.Logger.Info("Running common preflight preps...")
	err = preflights.Common()
	if err != nil {
		return err
	}
	logutil.Logger.Info("Preflights passed, green lights across the board!")

	serverOptions := determineServerOptions()
	executablePath := filepath.Join(viper.GetString("TempDir"), "Binaries", "Win32", "FoxGame-win32-Shipping.exe")
	var gameCmd *exec.Cmd

	if runtime.GOOS == "linux" {
		err = startXvfb()
		if err != nil {
			return err
		}
		gameCmd = exec.Command("wine", executablePath, "server", serverOptions)
	} else {
		gameCmd = exec.Command(executablePath, "server", serverOptions)
	}

	gameCmd.Dir = filepath.Dir(executablePath)

	logutil.Logger.Info("Launching BLR!", zap.String("serverOptions", serverOptions), zap.String("executablePath", executablePath), zap.String("gameCmd.Dir", gameCmd.Dir))

	err = startProcessAndScan(gameCmd)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleLogs(viper.GetString("TempDir"))

	// Wait til routines are done running
	wg.Wait()

	return nil
}

func determineServerOptions() string {
	// They said you should keep it simple, so I did
	// I still miss this abomination https://gitlab.com/northamp/docker-blrevive/-/blob/3ed14f4fd5590643fe6df77e7066f215914f2dc0/src/mars/launcher.py#L38
	var ServerOptionsArray []string

	ServerOptionsArray = append(ServerOptionsArray, viper.GetString("GameSettings.Map"))

	if c := viper.GetString("GameSettings.ConfigName"); c != "" {
		ServerOptionsArray = append(ServerOptionsArray, fmt.Sprintf("?Config=%s", c))
	} else {
		logutil.Logger.Warn("No Blacklight: Revive configuration name was specified, expect problems")
	}
	if c := viper.GetString("GameSettings.ServerName"); c != "" {
		ServerOptionsArray = append(ServerOptionsArray, fmt.Sprintf("?Servername=%s", c))
	}
	if c := viper.GetString("GameSettings.Password"); c != "" {
		ServerOptionsArray = append(ServerOptionsArray, fmt.Sprintf("?GamePassword=%s", c))
	}
	if c := viper.GetString("GameSettings.Playlist"); c != "" {
		ServerOptionsArray = append(ServerOptionsArray, fmt.Sprintf("?Playlist=%s", c))
	}
	if c := viper.GetString("GameSettings.GameMode"); c != "" {
		ServerOptionsArray = append(ServerOptionsArray, fmt.Sprintf("?Game=FoxGame.FoxGameMP_%s", c))
	}
	if c := viper.GetString("GameSettings.NumBots"); c != "" {
		ServerOptionsArray = append(ServerOptionsArray, fmt.Sprintf("?NumBots=%s", c))
	}
	if c := viper.GetString("GameSettings.MaxPlayers"); c != "" {
		ServerOptionsArray = append(ServerOptionsArray, fmt.Sprintf("?MaxPlayers=%s", c))
	}
	if c := viper.GetString("GameSettings.TimeLimit"); c != "" {
		ServerOptionsArray = append(ServerOptionsArray, fmt.Sprintf("?TimeLimit=%s", c))
	}
	if c := viper.GetString("GameSettings.SCP"); c != "" {
		ServerOptionsArray = append(ServerOptionsArray, fmt.Sprintf("?SCP=%s", c))
	}
	if c := viper.GetString("GameSettings.Custom"); c != "" {
		logutil.Logger.Warn("Custom launch argument(s) specified. If the server has issues, start here", zap.String("GameSettings.Custom", c))
		ServerOptionsArray = append(ServerOptionsArray, c)
	}

	return strings.Join(ServerOptionsArray, "")
}

// Spawn a new process and pipe its stdout/stderr to the entrypoint's logs
func startProcessAndScan(Cmd *exec.Cmd) (err error) {
	processName := filepath.Base(Cmd.Path)

	// Create pipes to the process
	stdin, err := Cmd.StdinPipe()
	if err != nil {
		return err
	}

	stdout, err := Cmd.StdoutPipe()
	if err != nil {
		return err
	}

	stderr, err := Cmd.StderrPipe()
	if err != nil {
		return err
	}

	// Spin it up
	if err := Cmd.Start(); err != nil {
		return err
	}

	logutil.Logger.Info("Now listening to process stdout/stderr", zap.String("process", processName))
	scannerStdout := bufio.NewScanner(stdout)
	wg.Add(1)
	go func() {
		defer wg.Done()
		for scannerStdout.Scan() {
			logutil.Logger.Info(scannerStdout.Text(), zap.String("processName", processName), zap.String("channel", "stdout"))
		}
		if err := scannerStdout.Err(); err != nil {
			logutil.Logger.Error("Scanner error", zap.String("processName", processName), zap.String("channel", "stdout"), zap.Error(err))
		}
	}()

	scannerStderr := bufio.NewScanner(stderr)
	wg.Add(1)
	go func() {
		defer wg.Done()
		for scannerStderr.Scan() {
			logutil.Logger.Error(scannerStderr.Text(), zap.String("processName", processName), zap.String("channel", "stderr"))
		}
		if err := scannerStderr.Err(); err != nil {
			logutil.Logger.Error("Scanner error", zap.String("processName", processName), zap.String("channel", "stderr"), zap.Error(err))
		}
	}()

	// And now we wait
	wg.Add(1)
	go func() {
		defer stdin.Close()
		defer stdout.Close()
		defer stderr.Close()
		err := Cmd.Wait()
		if err != nil {
			return
		}
		logutil.Logger.Fatal("Subprocess died! Aborting...", zap.String("process", processName), zap.Int("exitcode", Cmd.ProcessState.ExitCode()))
		wg.Done()
	}()

	return err
}

// Watch for new files in the predetermined log folder and output them through the entrypoint's log
func handleLogs(GamePath string) {
	defer wg.Done()

	// Create fsnotify watcher
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		logutil.Logger.Error("Failed to create fsnotify watcher", zap.Error(err))
		return
	}
	defer watcher.Close()

	logPath := filepath.Join(GamePath, "FoxGame", "Logs")

	done := make(chan bool)

	logutil.Logger.Debug("Starting watch over the logs folder", zap.String("logpath", logPath))

	wg.Add(1)
	go func() {
	watchloop:
		for {
			select {
			case event := <-watcher.Events:
				if event.Op == fsnotify.Create {
					logutil.Logger.Info("A new file was created in the log folder", zap.String("logfile", filepath.Base(event.Name)))
					wg.Add(1)
					go tailLogToStdout(event.Name)
				}

			case err := <-watcher.Errors:
				logutil.Logger.Error("Log watcher encountered an error", zap.Error(err))
				break watchloop
			}
		}
		wg.Done()
	}()

	if err := watcher.Add(logPath); err != nil {
		logutil.Logger.Error("Log watcher encountered an error", zap.Error(err))
	}

	<-done
}

func tailLogToStdout(LogFile string) {
	defer wg.Done()

	t, err := tail.TailFile(
		LogFile, tail.Config{Follow: true, ReOpen: true, MustExist: true})
	if err != nil {
		logutil.Logger.Error("Error during log tailing to stdout", zap.Error(err), zap.String("logfile", filepath.Base(LogFile)))
		return
	}

	for line := range t.Lines {
		logutil.Logger.Info(line.Text, zap.String("logfile", filepath.Base(LogFile)))
	}
}

// Start xvfb
func startXvfb() (err error) {
	// Very hack-y statement to free the Xvfb lock if the container was restarted before Xvfb freed it
	err = os.Remove("/tmp/.X9874-lock")
	if err != nil {
		// TODO: fix blatant lie :)
		logutil.Logger.Debug("No Xvfb lock file to report", zap.String("display", ":9874"), zap.String("file", "/tmp/.X9874-lock"))
	} else {
		logutil.Logger.Warn("Had to remove a Xvfb lock file", zap.String("display", ":9874"), zap.String("file", "/tmp/.X9874-lock"))
	}
	// Would be nice to use -displayfd instead of an arbitrary display number I suppose:
	XvfbCmd := exec.Command("Xvfb", ":9874", "-screen", "0", "1024x768x16")

	// Would be something like:
	// f, err := os.OpenFile("/tmp/xvfb", os.O_RDWR|os.O_CREATE, 0755)
	// if err != nil {
	// 		log.Fatal(err)
	// }
	// XvfbCmd := exec.Command("Xvfb", "-displayfd", "3", "-screen", "0", "1024x768x16")
	// XvfbCmd.ExtraFiles = []*os.File{f}

	// Matter is that it removes reliance on /tmp/.X9874-lock to rely on /tmp/xvfb instead... Not quite optimal.

	err = startProcessAndScan(XvfbCmd)
	if err != nil {
		return err
	}

	os.Setenv("DISPLAY", ":9874")
	logutil.Logger.Debug("Started Xvfb successfully", zap.String("display", ":9874"))

	return nil
}
