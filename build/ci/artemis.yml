include:
  - component: ${CI_SERVER_FQDN}/components/sast/sast@2.0.1
  - component: ${CI_SERVER_FQDN}/components/go/test@0.4.0
  - component: ${CI_SERVER_FQDN}/components/go/format@0.4.0
    inputs:
      stage: test

.release:
  image: registry.gitlab.com/blrevive/common/semrel
  script:
    # Copy script to a temporary location to ensure it exists at path declared in the semantic-release config
    - cp ${CI_PROJECT_DIR}/update-version.sh /tmp/update-version.sh && chmod +x /tmp/update-version.sh
    - cp ${CI_PROJECT_DIR}/publish.sh /tmp/publish.sh && chmod +x /tmp/publish.sh
    # Update .releaserc.json assets
    - |
      ASSETS_STR="\"assets\": ["
      first=true

      for FILE in ${CI_PROJECT_DIR}/bin/*; do
          if [ "$first" = true ]; then
              ASSETS_STR="${ASSETS_STR} {\"path\": \"${FILE}\"}"
              first=false
          else
              ASSETS_STR="${ASSETS_STR}, {\"path\": \"${FILE}\"}"
          fi
      done

      ASSETS_STR="${ASSETS_STR}]"
      echo "${ASSETS_STR}"
    - |
      sed -i 's@\"assets\": \[\]@'"$ASSETS_STR"'@' ".releaserc.json"
    - semantic-release ${DRY_RUN:+--dry-run}
  rules:
    # Run on default branch or beta
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH == "beta"

prerelease:dry-run:
  stage: prerelease
  extends:
    - .release
  variables:
    DRY_RUN: "true"
  artifacts:
    reports:
      dotenv: version.dotenv

.build:
  stage: build
  image: golang:latest
  script:
    - mkdir -p build
    - |
      go build -ldflags "\
      -X gitlab.com/northamp/artemis-blrevive/internal/pkg/version.version=${RELEASE_VERSION} \
      -X gitlab.com/northamp/artemis-blrevive/internal/pkg/version.metadata=${CI_PIPELINE_ID} \
      -X gitlab.com/northamp/artemis-blrevive/internal/pkg/version.gitCommit=${CI_COMMIT_SHA} \
      -X gitlab.com/northamp/artemis-blrevive/internal/pkg/version.gitTreeState=$(test -n "`git status --porcelain`" && echo "dirty" || echo "clean") \
      -X gitlab.com/northamp/artemis-blrevive/internal/pkg/version.buildTimestamp=$(date +%s) \
      " -o bin/artemis-${GOOS}-${GOARCH}${EXTENSION} gitlab.com/northamp/artemis-blrevive
  artifacts:
    paths:
      - bin/*
    expire_in: 1 week

build:linux-arm:
  extends: .build
  before_script:
    # Disabled to accomodate alpine's musl...
    - export CGO_ENABLED=0
    - export GOOS=linux
    - export GOARCH=arm

build:linux-386:
  extends: .build
  before_script:
    # Disabled to accomodate alpine's musl...
    - export CGO_ENABLED=0
    - export GOOS=linux
    - export GOARCH=386

build:linux-amd64:
  extends: .build
  before_script:
    # Disabled to accomodate alpine's musl...
    - export CGO_ENABLED=0
    - export GOOS=linux
    - export GOARCH=amd64

build:windows-386:
  extends: .build
  before_script:
    - export GOOS=windows
    - export GOARCH=386
    - export EXTENSION=.exe

build:windows-amd64:
  extends: .build
  before_script:
    - export GOOS=windows
    - export GOARCH=amd64
    - export EXTENSION=.exe

publish:release:
  stage: publish
  extends:
    - .release

# Fire a docker-blrevive pipeline
package:trigger image build:
  stage: package
  image: alpine:latest
  variables:
    DOCKER_BLREVIVE_PROJECT_ID: "39456850"
    # DOCKER_BLREVIVE_PIPE_TOKEN:
    DOCKER_BLREVIVE_REF_NAME: "master"
  before_script:
    - apk add curl
  script:
    - if [ -z "$RELEASE_VERSION" ]; then echo "RELEASE_VERSION is unset; probably means no release was created so don't need to create a container"; exit 0; fi
    - curl -X POST --fail -F token=${DOCKER_BLREVIVE_PIPE_TOKEN} -F ref=${DOCKER_BLREVIVE_REF_NAME} -F "variables[ARTEMIS_VERSION]=${RELEASE_VERSION}" ${CI_API_V4_URL}/projects/${DOCKER_BLREVIVE_PROJECT_ID}/trigger/pipeline
  rules:
    # Run on default branch or beta
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH == "beta") && $DOCKER_BLREVIVE_PIPE_TOKEN
