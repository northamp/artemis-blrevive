# Artemis - A Blacklight: Revive server launcher

**See also [BLREdit](https://github.com/halomaxx/blredit) for a client counterpart**

Starting a Blacklight: Revive server necessitates some setup, even more so when running on Linux. The [Blacklight: Revive Docker image](https://gitlab.com/northamp/docker-blrevive) and its entrypoint took care of that on Linux.

This launcher builds upon that entrypoint. It is still used in the container image, but can now also be used outside, **supports Windows setups**, and provides the following features:

* Manage modules installations
* Facilitate configuration management
  * For modules themsevles
  * For the server
* Facilitate log-forwarding by outputting everything the game creates to stdout

It also works in an ephemeral directory which is a carbon-copy of the game's, allowing read-only setups and mount-points to work and facilitating server maintainance by preventing previous installation residues from contaminating upgraded instances.

In the future, it should also:

* Ensure the game is in a conform state through integrity checks
* Expose the server's CLI to external connections

## Download

The launcher lives in the repository's [Package Registry](https://gitlab.com/northamp/artemis-blrevive/-/packages). Grab the executable appropriate to your platform and CPU architecture of the version of your choice, and you're good to go.

## Platform-specific considerations

On Windows, **you'll need to run the program as administrator**. This is because the launcher [uses Symbolic Links](pkg/symlinks/symlinks.go#L94) to create the ephemeral directory the game will run in, which requires administrator rights. This will probably change in the future, as VFS might be used instead.

On Linux, the launcher expects to find [Xvfb](https://www.x.org/releases/X11R7.6/doc/man/man1/Xvfb.1.xhtml) (since it assumes you want to run the game headless) and [Wine](https://www.winehq.org/) in your `$PATH`. Wine also requires DirectX to be installed, which can be done using `winetricks d3dx9 xact`.

## Configuration

This section is only about Artemis' configuration. For help on how to get the game, modules, etc... set up, check out the [Wiki](https://blrevive.gitlab.io/wiki/).

Run `artemis --help` for a list of the available subcommands, and `artemis [command] --help` for help on that specific command.

Flags listed by the help section can be configured by using a JSON file, the CLI arguments, environment variables, or a combination of them (in order of priority, JSON overriding all).

### With a JSON file

This method takes precedence over CLI, env vars or built-in defaults, and doesn't have to be entirely filled.

Run Artemis with `-c artemis.json` where artemis.json is the path to your file to use one. Each flags available in a command can be configured by following the dash structure, i.e.:

```bash
artemis start --log-json \
  --dir-blr /srv/blacklightre/game \
  --dir-blre /srv/blacklightre/blr \
  --dir-blreconfig /srv/blacklightre/config \
  --dir-modules /srv/blacklightre/modules \
  --dir-redist /srv/blacklightre/redist \
  --gamesettings-configname "blre" \
  --gamesettings-servername "Artemis+powered+BLRE+server" \
  --gamesettings-scp 123
```

Becomes:

```json
{
    "Log": {
      "Json": true,
    },
    "Dirs": {
      "BLR": "/srv/blacklightre/game",
      "BLRE": "/srv/blacklightre/blre",
      "BLREConfig": "/srv/blacklightre/config",
      "Modules": "/srv/blacklightre/modules",
      "Redist": "/srv/blacklightre/redist"
    },
    "GameSettings": {
      "ConfigName": "blre",
      "ServerName": "Artemis+powered+BLRE+server",
      "SCP": 123,
    }
}
```

### Using environment variables

The same configuration elements can be set using env vars by prefixing them with `BLREVIVE_`, writing them upper case (*not mandatory but preferable*), and adding underscores between nested elements, i.e.:

```bash
export BLREVIVE_LOG_JSON="true"
export BLREVIVE_DIRS_BLR="/srv/blacklightre/game"
export BLREVIVE_GAMESETTINGS_CONFIGNAME="blre"
export BLREVIVE_GAMESETTINGS_SERVERNAME="Artemis+powered+BLRE+server"
```

... And so on.
