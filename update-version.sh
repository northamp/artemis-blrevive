#!/bin/sh

PKG_VERSION=$1

# write version to a file to be consumed by dotenv
echo "RELEASE_VERSION=${PKG_VERSION}" > ${CI_PROJECT_DIR}/version.dotenv
